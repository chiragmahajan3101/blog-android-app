package com.example.blogapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.core.text.htmlEncode
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.blogapp.http.Blog
import com.example.blogapp.http.BlogHttpClient
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_blog_details.*
import okhttp3.internal.format

class BlogDetailsActivity : AppCompatActivity() {

    companion object{
        private const val EXTRAS_BLOG = "EXTRAS_BLOG"

        fun start(activity: Activity, blog: Blog) {
            val intent = Intent(activity, BlogDetailsActivity::class.java)
            intent.putExtra(EXTRAS_BLOG, blog)
            activity.startActivity(intent)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog_details)


        //GLIDE
        //FRESCO
        //PICASSO




        imageBack.setOnClickListener{ finish() }

        intent.extras?.getParcelable<Blog>(EXTRAS_BLOG)?.let{blog->
            showData(blog)
        }

    }


    private fun showData(blog: Blog) {
        progressBar.visibility = View.INVISIBLE
        textTitle.text = blog.title
        textDate.text = blog.date
        textAuthor.text = blog.author.name
        textRating.text = blog.rating.toString()
        textViews.text = String.format("(%d views",blog.views)
        textDecription.text = Html.fromHtml(blog.description)
        ratingBar.rating = blog.rating

        Glide.with(this)
            .load(blog.getImageUrl())
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(imageMain)

        Glide.with(this)
            .load(blog.author.getavatarUrl())
            .transform(CircleCrop())
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(imageAvatar)

    }

}